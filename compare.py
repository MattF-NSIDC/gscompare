import glob
import os
import re

from ipywidgets import IntText, Text, Select, Button, HBox, VBox, interact, Output
import matplotlib.pyplot as plt
import numpy as np

this_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(this_dir, 'data')

fig_layout = {
    (0, 0): 'a',
    (0, 1): 'b',
    (1, 0): 'a-b',
    (1, 1): 'b-a'
}

colormaps = ['viridis', 'magma', 'plasma', 'inferno']


def make_plot(data, title='', layout=fig_layout, colormap='magma'):
    plt.close('all')

    f, axarr = plt.subplots(2, 2, figsize=(9, 9), num=title)
    for fig_pos, fig_name in layout.items():
        subpl = axarr[fig_pos[0], fig_pos[1]]
        im = subpl.imshow(data[fig_name], cmap=colormap)
        subpl.set_title(fig_name)
        f.colorbar(im, ax=subpl)

    return plt


def file_to_arr(filepath, dtype, rows, cols):
    with open(filepath, 'rb') as f:
        return np.fromfile(f, dtype=dtype).reshape(rows, cols)


def load_filenames(dir_a, dir_b, regex=None):
    dir_a = os.path.join(data_dir, dir_a)
    dir_b = os.path.join(data_dir, dir_b)

    files_a = os.listdir(dir_a)
    files_b = os.listdir(dir_b)

    if regex:
        files_a = [re.search(regex, f).group(1) for f in files_a]
        files_b = [re.search(regex, f).group(1) for f in files_b]

    return sorted(set(files_a).union(files_b))


def file_from_pattern(path, pattern):
    path = os.path.join(data_dir, path)

    files = glob.glob(path + '/*' + pattern)
    if len(files) > 1:
        raise RuntimeError('More than one file returned for pattern "{}": {}'.format(pattern))

    return files[0]


def compare_directories(dir_a, dir_b, rows, cols, file_ids):
    results = {}
    for f in file_ids:
        dtype = np.int16 if f.endswith('.tim') else np.uint16
        file_a = file_from_pattern(dir_a, f)
        file_b = file_from_pattern(dir_b, f)

        a = file_to_arr(file_a, dtype, rows, cols)
        b = file_to_arr(file_b, dtype, rows, cols)
        a_minus_b = np.subtract(a.astype(np.int16), b.astype(np.int16))
        b_minus_a = np.subtract(b.astype(np.int16), a.astype(np.int16))

        data = {'a': a, 'b': b, 'a-b': a_minus_b, 'b-a': b_minus_a}
        results[f] = data

    return results


class GUI():
    file_ids = []
    comparison_results = []

    def __init__(self, dir_a='', dir_b='', rows=721, cols=721, file_match_regex=None):
        self.dir_a = Text(value=dir_a, description='Directory A:')
        self.dir_b = Text(value=dir_b, description='Directory B:')
        self.rows = IntText(value=rows, description='Rows:')
        self.cols = IntText(value=cols, description='Cols:')

        self.file_match_regex = Text(value=file_match_regex, description='Unique file match regex:')
        self.colormap = Select(options=colormaps, description='Colormap:')

        self.update_button = Button(description='Update', tooltip='Click to update')
        self.update_button.on_click(lambda _: self.update())
        self.interact_widget = None
        self.display_widget = Output()

        self.plot = plt.plot([])
        self.update()

    def update(self):
        self.file_ids = load_filenames(self.dir_a.value, self.dir_b.value, regex=self.file_match_regex.value)
        self.comparison_results = compare_directories(
                self.dir_a.value,
                self.dir_b.value,
                self.rows.value,
                self.cols.value,
                self.file_ids)

        if self.interact_widget:
            self.interact_widget.update()

    def new_plot(self, granule):
        data = self.comparison_results[granule]
        self.plot = make_plot(data, title=granule, colormap=self.colormap.value)

    def show(self):
        left = VBox([self.dir_a, self.dir_b, self.colormap])
        right = VBox([self.rows, self.cols, self.file_match_regex])
        with self.display_widget:
            self.interact_widget = interact(self.new_plot, granule=self.file_ids).widget

        return VBox([HBox([left, right]), self.update_button, self.display_widget])


if __name__ == '__main__':
    dir_a = os.path.join('data', 'snow')
    dir_b = os.path.join('data', 'this_vm')
    compare_directories(dir_a, dir_b)

import os
import pprint
from unittest import TestCase

import numpy as np
import numpy.testing


def file_to_arr(filepath, dtype, rows, cols):
    with open(filepath, 'rb') as f:
        return np.fromfile(f, dtype=dtype).reshape(rows, cols)


def print_differences(diff_coords, a1, a2):
    for index, diff_coord in enumerate(diff_coords):
        col, row = diff_coord
        print("({}) Difference at {}. VM: {}; Snow: {}".format(index, diff_coord, a1[col, row], a2[col, row]))



num_cells = 721 * 721
results = {}
filenames = os.listdir('data/snow')

for filename in filenames:
    print("=> Processing {}".format(filename))

    snow_file = os.path.join('data', 'snow', filename)
    vm_file = os.path.join('data', 'this_vm', filename)
    snow_arr = file_to_arr(snow_file, np.uint16, 721, 721)
    vm_arr = file_to_arr(vm_file, np.uint16, 721, 721)

    diff = np.subtract(snow_arr, vm_arr)
    nonzero = np.nonzero(diff)
    diff_coords = [(nonzero[0][index], nonzero[1][index]) for index, _ in enumerate(nonzero[0])]
    diff_count = len(diff_coords)
    diff_pct = "{0:.5f}".format(diff_count / num_cells)
    print("Found {} differences ({}%)".format(diff_count, diff_pct))
    print()

    results[filename] = len(diff_coords)

    #print_differences(diff_coords, vm_arr, snow_arr)


pprint.pprint(results)
